from plugins.types.i_plugin import IPlugin


plug = IPlugin({
                "uid": None,
                "name": "string",
                "sql_type": "varchar(50)",
                "data": "[{'name': 'Default value', 'type': 'string'},"
                        " {'name': 'Required', 'type': 'bool'}]",
                "version": "0.0.1",
                "close_date": None})

