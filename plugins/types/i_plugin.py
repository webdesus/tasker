import json
from util import Structure

default = {
    "uid": None,
    "name": None,
    "sql_type": None,
    "data": None,
    "version": None,
    "close_date": None
}


class IPlugin(Structure):
    def __init__(self, data=default):
        Structure.__init__(self, default, data, "IPlugin")

    def json_data(self):
        j_data = json.loads(self.data)
        return j_data
