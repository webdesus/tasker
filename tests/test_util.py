import unittest
from tests.common import TestCase
from dao.tables.type_modules import TypeModules as T_dic
from util import Structure


class TestStructure(TestCase):

    test_values = {
        "uid": None,
        "name": "test",
        "sql_type": "test",
        "data": "test",
        "version": "test",
        "close_date": None
    }

    def test_01_structure_raises(self):

        class TestTypeMod(Structure):
            def __init__(self, data=TestStructure.test_values):
                Structure.__init__(self, TestStructure.test_values, data)

        msg_type = "Structure Checker: The sent object is not a type of 'TypeModules'!"
        msg_field = "Structure Checker: Field 'obj_name' is required!"

        with self.assertRaisesRegex(Exception, msg_type):
            del TestStructure.test_values["uid"]
            T_dic(TestStructure.test_values)
        with self.assertRaisesRegex(Exception, msg_type):
            TestStructure.test_values["no_name"] = TestStructure.test_values.pop("name")
            T_dic(TestStructure.test_values)
        with self.assertRaisesRegex(Exception, msg_field):
            TestTypeMod()

    def test_02_structure_positive(self):
        class TestTypeMod(Structure):
            def __init__(self, data=TestStructure.test_values):
                Structure.__init__(self, TestStructure.test_values, data, "test object name")
        self.assertTrue(TestTypeMod())


if __name__ == '__main__':
    unittest.main()