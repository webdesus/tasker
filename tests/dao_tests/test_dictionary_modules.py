import unittest
from tests.common import TestCase, get_sql_items, create_default_db
from dao.util import Util
from dao.tables.dictionary_modules import DictionaryModules as Dm_dic
from config import Config
from dao.dictionary_modules import DictionaryModules
from util import Structure


class TestDictionaryModules(TestCase):
    Util.db_connect = Config.TEST_DB_CONNECTION
    test_values = Dm_dic({
        "uid": None,
        "dictionary_id": "test",
        "module_id": "test",
        "data": "{'test': 'test'}",
        "sort": "test"
    })

    def test_insert_get(self):

        def callback(cursor):
            create_default_db(cursor)
            expected_obj = TestDictionaryModules.test_values
            expected_obj.uid = DictionaryModules().insert(expected_obj, cursor)
            result = DictionaryModules().get(cursor)
            actual_obj = Dm_dic(dict(result[0]))
            self.assert_equal_obj(expected_obj, actual_obj)

        Util.db_action(callback)

    def test_class_obj(self):
        self.assertIsInstance(TestDictionaryModules.test_values, cls=Structure)


if __name__ == '__main__':
    unittest.main()