import unittest
from config import Config
from tests.common import TestCase, get_sql_items, create_default_db
from dao.util import Util
from dao.dictionary import Dictionary
from dao.tables.dictionary import Dictionary as D_dic
from datetime import datetime
from util import Structure


class TestDictionary(TestCase):
    Util.db_connect = Config.TEST_DB_CONNECTION
    test_values = D_dic({
        "uid": None,
        "name": "test name",
        "open_date": None,
        "close_date": None
    })

    def test_insert_get(self):

        def callback(cursor):
            create_default_db(cursor)
            expected_obj = TestDictionary.test_values
            expected_obj.uid = Dictionary().insert(expected_obj, cursor)
            expected_obj.open_date = get_sql_items("SELECT open_date FROM 'Core.Dictionary'", cursor)[0]["open_date"]
            result = Dictionary().get(cursor)
            actual_obj = D_dic(dict(result[0]))
            self.assert_equal_obj(expected_obj, actual_obj)

        Util.db_action(callback)

    def test_remove(self):

        def callback(cursor):
            create_default_db(cursor)
            uid = Dictionary().insert(TestDictionary.test_values, cursor)
            Dictionary().remove(uid, cursor)
            expected_close_date = datetime.now().strftime('%Y-%m-%d %H:%M')
            actual_close_date = get_sql_items("SELECT close_date FROM 'Core.Dictionary'", cursor)[0]["close_date"]
            self.assertEqual(expected_close_date, actual_close_date)

        Util.db_action(callback)

    def test_class_obj(self):
        self.assertIsInstance(TestDictionary.test_values, cls=Structure)


if __name__ == '__main__':
    unittest.main()