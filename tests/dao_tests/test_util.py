import unittest
import copy
from unittest.mock import patch
from tests.common import TestCase, create_default_db
from config import Config
from dao.util import Util
from logic.modules import LogicMod
from dao.type_modules import TypeModules
from plugins.types.i_plugin import IPlugin


dir_data = LogicMod.registration()
Util.db_connect = Config.TEST_DB_CONNECTION


def any_type(cls):
    class Any(cls):
        def __eq__(self, other):
            print(other)
            print(cls)
            return isinstance(other, cls)
    return Any()


def generate_db_data(cursor):
    create_default_db(cursor)
    for key in list(dir_data.keys()):
        TypeModules().insert(dir_data[key], cursor)
    result = TypeModules().get(cursor)
    return result


test_data = Util.db_action(generate_db_data)


class TestUtil(TestCase):

    @patch("dao.type_modules.TypeModules.insert_update")
    @patch("dao.type_modules.TypeModules.get")
    @patch("dao.type_modules.TypeModules.insert")
    def test_registration(self, insert_mock, get_mock, insert_update_mock):
        get_mock.return_value = {}
        insert_update_mock.return_value = "test_uid"
        Util.registration(dir_data)
        self.assertTrue(insert_mock.called)

    @patch("dao.type_modules.TypeModules.get")
    @patch("dao.type_modules.TypeModules.insert_update")
    def test_01_reg_version_up(self, insert_update_mock, get_mock):
        dir_data_test = copy.deepcopy(dir_data)
        dir_data_test["text"].version = "0.0.2"
        get_mock.return_value = test_data
        Util.registration(dir_data_test)
        self.assertTrue(insert_update_mock.called)
        insert_update_mock.assert_called_with(any_type(IPlugin), any_type(str), any_type(object))

    @patch("dao.type_modules.TypeModules.get")
    @patch("dao.type_modules.TypeModules.insert_update")
    def test_reg_version_down(self, insert_update_mock, get_mock):
        dir_data_test = copy.deepcopy(dir_data)
        dir_data_test["text"].version = "0.0.0"
        get_mock.return_value = test_data
        Util.registration(dir_data_test)
        self.assertTrue(insert_update_mock.called)
        insert_update_mock.assert_called_with(any_type(IPlugin), any_type(str), any_type(object))

    @patch("dao.type_modules.TypeModules.get")
    @patch("dao.type_modules.TypeModules.insert_update")
    def test_reg_version_pass(self, insert_update_mock, get_mock):
        get_mock.return_value = test_data
        Util.registration(dir_data)
        insert_update_mock.assert_not_called()


if __name__ == '__main__':
    unittest.main()