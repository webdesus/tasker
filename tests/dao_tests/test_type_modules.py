import unittest
import copy
from config import Config
from dao.type_modules import TypeModules
from dao.util import Util
from dao.tables.type_modules import TypeModules as T_dic
from tests.common import TestCase, get_sql_items, create_default_db
from util import Structure
from datetime import datetime


class TestTypeModules(TestCase):
    Util.db_connect = Config.TEST_DB_CONNECTION
    test_values = T_dic({
        "uid": None,
        "name": "test",
        "sql_type": "test",
        "data": "test",
        "version": "0.0.1",
        "close_date": None
    })

    def test_insert_get(self):

        def callback(cursor):
            create_default_db(cursor)
            expected_obj = TestTypeModules.test_values
            expected_obj.uid = TypeModules().insert(expected_obj, cursor)
            result = TypeModules().get(cursor)
            actual_obj = T_dic(dict(result["test"]))
            self.assert_equal_obj(expected_obj, actual_obj)

        Util.db_action(callback)

    def test_remove(self):

        def callback(cursor):
            create_default_db(cursor)
            TypeModules().insert(TestTypeModules.test_values, cursor)
            TypeModules().remove(TestTypeModules.test_values.name, cursor)
            expected_close_date = datetime.now().strftime('%Y-%m-%d %H:%M')
            actual_close_date = get_sql_items("SELECT close_date FROM 'Core.TypeModules'", cursor)[0]["close_date"]
            self.assertEqual(expected_close_date, actual_close_date)

        Util.db_action(callback)

    def test_insert_update(self):

        def callback(cursor):
            create_default_db(cursor)
            uid = TypeModules().insert(TestTypeModules.test_values, cursor)
            TestTypeModules.test_values.version = "0.0.2"
            TypeModules().insert_update(TestTypeModules.test_values, uid, cursor)
            expected_close_date = datetime.now().strftime('%Y-%m-%d %H:%M')
            db_items = get_sql_items("SELECT uid, close_date FROM 'Core.TypeModules' "
                                     "WHERE close_date IS NOT NULL", cursor)
            actual_close_date = db_items[0]["close_date"]
            check_uid = db_items[0]["uid"]
            self.assertEqual(uid, check_uid)
            self.assertEqual(expected_close_date, actual_close_date)

        Util.db_action(callback)

    def test_insert_exception(self):

        def callback(cursor):
            create_default_db(cursor)
            test_values = copy.copy(TestTypeModules.test_values)
            test_values.version = "test"
            msg = "test is not valid SemVer string"
            with self.assertRaisesRegex(Exception, msg):
                TypeModules().insert(test_values, cursor)

        Util.db_action(callback)

    def test_insert_update_exception(self):

        def callback(cursor):
            create_default_db(cursor)
            test_values = copy.copy(TestTypeModules.test_values)
            test_values.version = "test"
            msg = "test is not valid SemVer string"
            uid = "test"
            with self.assertRaisesRegex(Exception, msg):
                TypeModules().insert_update(test_values, uid, cursor)

        Util.db_action(callback)

    def test_class_obj(self):
        self.assertIsInstance(TestTypeModules.test_values, cls=Structure)


if __name__ == '__main__':
    unittest.main()