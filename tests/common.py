import unittest


class TestCase(unittest.TestCase):

    def assert_equal_obj(self, obj_1, obj_2):
        self.assertEqual(obj_1.__dict__, obj_2.__dict__)


def get_sql_items(query, cursor, condition=None):
    if condition is not None:
        cursor.execute(query, condition)
    else:
        cursor.execute(query)
    obj_list = cursor.fetchall()
    for obj in obj_list:
        if "name" in obj.keys():
            result = {obj["name"]: obj for obj in obj_list}
            return result
    else:
        return obj_list


def create_default_db(cursor):
    cursor.executescript("""
    CREATE TABLE 'Core.Dictionary' (
        uid        CHAR (36),
        name       VARCHAR (50),
        open_date  DATETIME,
        close_date DATETIME);
    CREATE TABLE 'Core.DictionaryModules' (
        uid           CHAR (36),
        dictionary_id CHAR (36),
        module_id     CHAR (36),
        data          JSON,
        sort          [INT UNSIGNED] NOT NULL);
    CREATE TABLE 'Core.TypeModules' (
        uid        CHAR (36),
        name       VARCHAR (50),
        sql_type   VARCHAR (50),
        data       VARCHAR (250),
        version    VARCHAR (15),
        close_date DATETIME (50) );
    """)
