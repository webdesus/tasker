class Structure(object):
    def __init__(self, default, data, obj_name=None):
        if obj_name is None:
            raise Exception("Structure Checker: Field 'obj_name' is required!")
        elif len(default) != len(data):
            raise Exception("Structure Checker: The sent object is not a type of '{0}'!". format(obj_name))
        for key in default.keys():
            if key not in data:
                raise Exception("Structure Checker: The sent object is not a type of '{0}'!". format(obj_name))
            self.__dict__.update(**data)
