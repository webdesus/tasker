import uuid
from datetime import datetime


class Dictionary(object):

    def insert(self, dic, cursor):
        uid = str(uuid.uuid4())
        cursor.execute("INSERT INTO 'Core.Dictionary' (uid, name, open_date)"
                       "VALUES (:uid, :name, :open_date)", {"uid": uid,
                                                            "name": dic.name,
                                                            "open_date": datetime.now().strftime('%Y-%m-%d %H:%M')})
        return uid

    def get(self, cursor):
        cursor.execute("SELECT * FROM 'Core.Dictionary'")
        obj_list = cursor.fetchall()  # TODO how to return it?
        return obj_list

    def remove(self, uid, cursor):
        cursor.execute("UPDATE 'Core.Dictionary' "
                       "SET close_date = (:close_date) "
                       "WHERE uid = (:uid)", {"close_date": datetime.now().strftime('%Y-%m-%d %H:%M'),
                                              "uid": uid})
