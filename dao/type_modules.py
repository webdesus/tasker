import uuid
import semver
from datetime import datetime


class TypeModules(object):

    def insert(self, dic, cursor):
        uid = str(uuid.uuid4())
        if semver.parse(dic.version):
            cursor.execute("INSERT INTO 'Core.TypeModules'(uid, name, sql_type, data, version) "
                           "VALUES (:uid, :name, :sql_type, :data, :version)",
                           {"uid": uid, "name": dic.name, "sql_type": dic.sql_type,
                            "data": dic.data, "version": dic.version})
            return uid

    def get(self, cursor):
        cursor.execute("SELECT * FROM 'Core.TypeModules'")
        obj_list = cursor.fetchall()
        result = {obj["name"]: obj for obj in obj_list}
        return result

    def insert_update(self, dic, old_uid, cursor):
        uid = str(uuid.uuid4())
        if semver.parse(dic.version):
            cursor.execute("INSERT INTO 'Core.TypeModules' (uid, name, sql_type, data, version) "
                           "VALUES (:uid, :name, :sql_type, :data, :version)", {"uid": uid,
                                                                                "name": dic.name,
                                                                                "sql_type": dic.sql_type,
                                                                                "data": dic.data,
                                                                                "version": dic.version})
        cursor.execute("UPDATE 'Core.TypeModules'"
                       "SET close_date = (:close_date)"
                       "WHERE uid = (:old_uid)", {"close_date": datetime.now().strftime('%Y-%m-%d %H:%M'),
                                                  "old_uid": old_uid})

        return uid

    def remove(self, name, cursor):
        cursor.execute("UPDATE 'Core.TypeModules'"
                       "SET close_date = (:close_date)"
                       "WHERE name = (:name)", {"close_date": datetime.now().strftime('%Y-%m-%d %H:%M'),
                                                "name": name})


