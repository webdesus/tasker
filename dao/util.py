import sqlite3
import semver
from config import Config
from dao.type_modules import TypeModules


class Util(object):

    db_connect = Config.DB_CONNECTION

    @staticmethod
    def do_action(func):
        try:
            result = func()
            return result
        except sqlite3.Error as err:
            raise Exception("database error: " + str(err))
        except ValueError as err:
            raise Exception("Value error: " + str(err))
        except Exception as err:  # TODO with base exception
            raise err

    @staticmethod
    def db_action(func):
        def inside_func():
            conn = sqlite3.connect(Util.db_connect)
            conn.row_factory = sqlite3.Row
            cursor = conn.cursor()
            result_func = func(cursor)
            conn.commit()
            cursor.close()
            conn.close()
            return result_func
        result = Util.do_action(inside_func)
        return result

    @staticmethod
    def db_action_tran(self):
        pass

    @staticmethod
    def registration(dir_data):

        db_data = Util.db_action(lambda cursor: TypeModules().get(cursor))

        def check_version(key):
            db_ver = db_data[key]["version"]
            dir_ver = dir_data[key].version
            if semver.compare(dir_ver, db_ver) == 1:
                Util.db_action(lambda cursor: TypeModules().insert_update(dir_data[key], db_data[key]["uid"], cursor))
            elif semver.compare(dir_ver, db_ver) == -1:
                Util.db_action(lambda cursor: TypeModules().insert_update(dir_data[key], db_data[key]["uid"], cursor))
            else:
                pass  # TODO: it may be necessary need to return some msg

        for key in list(dir_data.keys()):
            if key not in list(db_data.keys()):
                Util.db_action(lambda cursor: TypeModules().insert(dir_data[key], cursor))  # insert
            else:
                check_version(key)

