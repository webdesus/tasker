from util import Structure

default = {
    "uid": None,
    "dictionary_id": None,
    "module_id": None,
    "data": None,
    "sort": None
}


class DictionaryModules(Structure):
    def __init__(self, data=default):
        Structure.__init__(self, default, data, "DictionaryModules")

