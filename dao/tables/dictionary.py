from util import Structure

default = {
    "uid": None,
    "name": None,
    "open_date": None,
    "close_date": None
}


class Dictionary(Structure):
    def __init__(self, data=default):
        Structure.__init__(self, default, data, "Dictionary")

