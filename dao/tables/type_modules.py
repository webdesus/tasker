from util import Structure

default = {
    "uid": None,
    "name": None,
    "sql_type": None,
    "data": None,
    "version": None,
    "close_date": None
}


class TypeModules(Structure):
    def __init__(self, data=default):
        Structure.__init__(self, default, data, "TypeModules")

