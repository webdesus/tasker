import uuid


class DictionaryModules(object):

    def insert(self, dic, cursor):
        uid = str(uuid.uuid4())
        cursor.execute("INSERT INTO 'Core.DictionaryModules' (uid, dictionary_id, module_id, data, sort)"
                       "VALUES (:uid, :dictionary_id, :module_id, :data, :sort)", {"uid": uid,
                                                                                   "dictionary_id": dic.dictionary_id,
                                                                                   "module_id": dic.module_id,
                                                                                   "data": dic.data,
                                                                                   "sort": dic.sort})
        return uid

    def get(self, cursor):
        cursor.execute("SELECT * FROM 'Core.DictionaryModules'")
        obj_list = cursor.fetchall()
        return obj_list
