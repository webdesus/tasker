import glob
import importlib.machinery
from pathlib import Path


class LogicMod(object):

    @staticmethod
    def registration():
        home_dir = Path().home()
        paths = glob.glob(str(home_dir) + "/**/types/plug_*.py", recursive=True)
        plugins_dict = {}

        for path in paths:
            loader = importlib.machinery.SourceFileLoader('plugin', path)
            plugin = loader.load_module().plug
            plugins_dict[plugin.name] = plugin
        return plugins_dict
